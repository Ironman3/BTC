<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>BTC</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('vendor\components\font-awesome\css\font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor\twbs\bootstrap\dist\css\bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor\datatables\datatables\media\css\jquery.dataTables.min.css') }}">
</head>

<body>
<div class="container">
    <label>HUF exchange rate:</label>
    {{ Form::open(['class' => 'chart-form', 'action' =>'ExchangeRateController@ajax_exchangeRate','method' => 'post']) }}
    <div class="form-group">
        <input type="number" name="huf_rate" min="0" class="form-control" placeholder="quantity in HUF">
    </div>
    <button type="submit" class="btn btn-secondary btn-sm btn-info">submit</button>
    {{ Form::close() }}
    <br>
</div>
<div class="container">
    <table id="table" class="table">
        <thead><tr><th>BTC BUY</th><th>BTC SELL</th><th>ETH BUY</th><th>ETH SELL</th><th>UPDATED AT</th></tr></thead>
        <tbody></tbody>
    </table>
</div>
<div id="chart" style="width:100%; height:400px;"></div>

<footer>
    <script src="{{ asset('vendor\code\highstock.js') }}"></script>
    <script src="{{ asset('vendor\components\jquery\jquery.min.js') }}"></script>
    <script src="{{ asset('vendor\twbs\bootstrap\dist\js\bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor\datatables\datatables\media\js\jquery.dataTables.min.js') }}"></script>
    <script>
        const base_url = '<?php echo url('') ?>';
    </script>
    {{ HTML::script("public/js/exchange_rate.js") }}
</footer>
</body>
</html>
