$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    window.setInterval(function() {

        $.ajax({
            url: base_url + "/update_exchange_data",
            type: "json",
            method: "POST",
            processData: true,
            data: {},
            cache: true,
            success: function (result_stream) {
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }, 5000);
});



$(document).on("submit", ".chart-form", function(event) {

    event.preventDefault();
    $(document.body).css("cursor", "wait");

    var form_data = new FormData(this);
    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: form_data,
        contentType: false,
        processData: false,
        cache: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf_token"]').attr('content'));
        },
        success: function (data) {

            if(data != null) {

                var rates = JSON.parse(data);
                reloadData(rates);
            }
        },
        complete: function () {
            $(document.body).css("cursor", "default");
        },
        error: function (data) {
        }
    });
});

function reloadData(rates) {

    var table_str = "";

    $.each(rates['updated_at'], function (key, item) {

        const item_date = new Date(item);
        const curr_date = new Date();

        const elpased_time = (curr_date - item_date) / (24 * 216000);
        if(elpased_time < 10) {

            table_str +=
                "<tr>" +
                "<td>" + rates['btc_buy'][key] + "</td>" +
                "<td>" + rates['btc_sell'][key] + "</td>" +
                "<td>" + rates['eth_buy'][key] + "</td>" +
                "<td>" + rates['eth_sell'][key] + "</td>" +
                "<td>" + rates['updated_at'][key] + "</td>" +
                "</tr>";
        }
    });

    $('#table').find("tbody").html(table_str);
    $('#table').DataTable();

    var myChart = Highcharts.chart('chart', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'exchange rate of HUF, BTC and ETH'
        },
        xAxis: {
            categories: rates['updated_at']
        },
        yAxis: {
            title: {
                text: 'Exchange rate'
            }
        },
        series: [
            {
                name: 'BTC_BUY',
                data: rates['btc_buy']
            },
            {
                name: 'BTC_SELL',
                data: rates['btc_sell']
            },
            {
                name: 'ETH_BUY',
                data: rates['eth_buy']
            },
            {
                name: 'ETH_SELL',
                data: rates['eth_sell']
            }
        ]
    });
}

