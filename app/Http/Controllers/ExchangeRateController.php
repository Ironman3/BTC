<?php

namespace App\Http\Controllers;

use App\Models\ExchangeRate;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ExchangeRateController extends Controller
{
    public function __construct()
    {
    }

    public function ajax_exchangeRate()
    {
        $hufRate = Input::get('huf_rate');
        session(['huf_rate' => $hufRate]);
        $filePath = public_path('/uploads/huf.txt');
        file_put_contents($filePath, [$hufRate]);

        $rateList = ExchangeRate::all();
        $btcBuyArray = [];
        $btcSellArray = [];
        $ethBuyArray = [];
        $ethSellArray = [];
        $updatedAtArray = [];

        $rateList->each(function($item, $key) use(
            &$btcBuyArray,
            &$btcSellArray,
            &$ethBuyArray,
            &$ethSellArray,
            &$updatedAtArray) {

            $btc = json_decode($item->btc);
            $eth = json_decode($item->eth);

            $btcBuyArray[] = $btc->ticker->buy * $item->huf;
            $btcSellArray[] = $btc->ticker->sell * $item->huf;
            $ethBuyArray[] = $eth->ticker->buy * $item->huf;
            $ethSellArray[] = $eth->ticker->sell * $item->huf;
            $updatedAtArray[] = (string)$item->updated_at;
        });

        echo json_encode([
            'btc_buy' => $btcBuyArray,
            'btc_sell' => $btcSellArray,
            'eth_buy' => $ethBuyArray,
            'eth_sell' => $ethSellArray,
            'updated_at' => $updatedAtArray
        ]);
    }

    public function ajax_updateExchangeData()
    {
        echo static::updateExchangeData(session('huf_rate', 0));
    }

    public static function updateExchangeData($huf)
    {
        $now = Carbon::now();
        DB::table('exchange_rates')->insert([
            'btc' => static::getBTCRate(),
            'eth' => static::getETHRate(),
            'huf' => !is_null($huf) ? $huf : 0,
            'created_at' => $now,
            'updated_at' => $now
        ]);
        return true;
    }

    protected static function getBTCRate()
    {
        $ch = curl_init("https://www.tidebit.com//api/v2/tickers/btcusd.json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }

    protected static function getETHRate()
    {
        $ch = curl_init("https://www.tidebit.com//api/v2/tickers/ethusd.json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }
}
